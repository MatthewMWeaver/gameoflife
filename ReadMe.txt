Conway's Game of Life Console Application v1.0
Developed by Matt Weaver for Cardinal Health FUSE

Summary:
Enclosed is a console based, command line driven application that works to simulate Conway's Game of Life. It is written in C# and developed and delivered as a Microsoft VisualStudio Solution folder. It also includes Unit Tests that demonstrate that the elements of the program behave as expected.

Program:
The program can be run simply by opening the solution file in VisualStudio and pressing run. The program begins in the main menu, where the user can choose to either set up a game, read the rules or exit the program. The first two options always loop back to this point.
Reading the rules simply displays the information before taking the user back to the main menu upon any keyboard input.
Setting up the game leads the user first to a setup page that takes and verifies input for the user's name, number of rows, number of columns, and number of Life Cycles. The user is then given the option to either set up the board or choose a random seed of the specified size.

Tests:
The VisualStudio solution also contains several unit tests to verify the accuracy of the program's representation of the rules of the game. These as well can be run in VisualStudio by going to Tests->Run->All Tests.
The purpose of each of these tests is outlined in a comment above the method. The primer tests were done first as to use them in the State Tests, and the CLI tests were done last as all other components were necessary.
