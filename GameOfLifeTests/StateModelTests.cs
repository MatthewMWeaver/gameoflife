﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameOfLife;
using System.Collections.Generic;

namespace GameOfLifeTests
{
    [TestClass]
    public class StateModelTests
    {
        //In the following tests, a 0 represents a dead cell and a 1 represents a living cell

        //Ensures that empty consructor will produce object and initialize Cells list
        [TestMethod]
        public void EmptyConstructorTest()
        {
            StateModel state = new StateModel();
            state.Cells.Add(new List<int>());
            state.Cells[0].Add(3);
            Assert.IsNotNull(state);
            Assert.AreEqual(3, state.Cells[0][0]);
        }

        //Ensures that constructor passed Primer object will produce object and pass traits as expected
        [TestMethod]
        public void PrimerConstructorTest()
        {
            Primer primer = new Primer();
            primer.PlayerName = "Test";
            primer.LifeCycles = 5;
            StateModel state = new StateModel(primer);
            state.Cells.Add(new List<int>());
            state.Cells[0].Add(3);
            Assert.AreEqual( "Test", state.PlayerName);
            Assert.AreEqual(5, state.LifeCycles);
            Assert.AreEqual(3, state.Cells[0][0]);
        }

        //Ensures that constructor passed with a previous state will produce a
        //state of the same row and column length
        [TestMethod]
        public void StateConstructorTest()
        {
            StateModel initial = new StateModel();
            List<int> dummyRow = new List<int>();
            dummyRow.AddRange(new int[] {
                0, 0, 0, 0
            });
            for (int i = 0; i < 5; i++)
            {
                initial.Cells.Add(dummyRow);
            }
            //At this point initial state should have 5 rows and 4 columns of dead cells
            StateModel testState = new StateModel(initial);
            //testState should now also have 5 rows and 4 columns of dead cells
            Assert.AreEqual(5, testState.Cells.Count);
            Assert.AreEqual(4, testState.Cells[0].Count);
            Assert.AreEqual(4, testState.Cells[1].Count);
            Assert.AreEqual(4, testState.Cells[2].Count);
            Assert.AreEqual(4, testState.Cells[3].Count);
            Assert.AreEqual(4, testState.Cells[4].Count);
        }

        //The following test is lengthy but ensures that the core logic of the kata is
        //working as intended. It uses the CountLivingNeighbors() method and checks to 
        //see if no matter what the position, the cells' surrounding neighbors' status can be obtained 
        [TestMethod]
        public void CountNeighborsTest()
        {
            StateModel initial = new StateModel();
            List<int> dummyRow = new List<int>();
            dummyRow.AddRange(new int[] {
                1, 1, 1
            });
            for (int i = 0; i < 3; i++)
            {
                initial.Cells.Add(dummyRow);
            }
            //At this point the initial state should have 3x3 grid of living cells
            //Executing CountLivingNeighbors() at coordinates 1, 1 (zero-based), the center cell
            //should return 8, demonstrating that it has checked and found 8 living cells neighboring it
            Assert.AreEqual(8, initial.CountLivingNeighbors(1, 1, initial));
            //Since this code is only supposed to check living neighbors, let's make sure that it does
            initial = new StateModel();
            dummyRow = new List<int>();
            dummyRow.AddRange(new int[] {
                1, 1, 0
            });
            for (int i = 0; i < 3; i++)
            {
                initial.Cells.Add(dummyRow);
            }
            //This method should return 5 living cells
            Assert.AreEqual(5, initial.CountLivingNeighbors(1, 1, initial));
        }

        //This is all well and good but not every cell will be surrounded by the standard 8 neighbors
        //There are 8 different edge cases to consider (literaly edge cases in this problem)
        //They are laid out as follows

        [TestMethod]
        public void EdgeCaseTest()
        {
            StateModel initial = new StateModel();
            List<int> dummyRow = new List<int>();
            dummyRow.AddRange(new int[] {
                1, 1, 1
            });
            for (int i = 0; i < 3; i++)
            {
                initial.Cells.Add(dummyRow);
            }
            //Asserts that only 5 cells are returned if the cell is in the top row
            Assert.AreEqual(5, initial.CountLivingNeighbors(0, 1, initial));
            //Asserts that only 5 cells are returned if the cell is in the bottom row
            Assert.AreEqual(5, initial.CountLivingNeighbors(2, 1, initial));
            //Asserts that only 5 cells are returned if the cell is in the leftmost column
            Assert.AreEqual(5, initial.CountLivingNeighbors(1, 0, initial));
            //Asserts that only 5 cells are returned if the cell is in the rightmost column
            Assert.AreEqual(5, initial.CountLivingNeighbors(1, 2, initial));
            //Asserts that only 3 cells are returned if the cell is in the top right corner
            Assert.AreEqual(3, initial.CountLivingNeighbors(0, 2, initial));
            //Asserts that only 3 cells are returned if the cell is in the top left corner
            Assert.AreEqual(3, initial.CountLivingNeighbors(0, 0, initial));
            //Asserts that only 3 cells are returned if the cell is in the bottom right corner
            Assert.AreEqual(3, initial.CountLivingNeighbors(2, 2, initial));
            //Asserts that only 3 cells are returned if the cell is in the bottom left corner
            Assert.AreEqual(3, initial.CountLivingNeighbors(2, 0, initial));
        }

        //At this point we are absolutely sure that the program will correctly count the number of
        //living neighbors, and using that and the initial state of the cell, we can use the 4 rules
        //to calculate the status of the cell of the same position in the next generation

        //Rules 1, 2 and 3 deal with living cells so for simplicitys sake, I will sart with rule 4
        //which states that a dead cell with exactly 3 living neighbors comes alive
        [TestMethod]
        public void DeadCellDeterminationTests()
        {
            StateModel test = new StateModel();
            //a dead cell with 2 living neighbors should return dead
            Assert.AreEqual(0, test.DetermineStatus(0, 2));
            //a dead cell with 3 living neighbors should return alive
            Assert.AreEqual(1, test.DetermineStatus(0, 3));
            //a dead cell with 4 living neighbors should return
            Assert.AreEqual(0, test.DetermineStatus(0, 4));
        }

        //Rules 1 and 2 specify that if a living cell has either less than 2 or more than 3
        //living neighbors, it will die, but at either 2 or 3 exactly, it will live
        [TestMethod]
        public void LivingCellDeterminationTests()
        {
            StateModel test = new StateModel();
            //a living cell with 0 living neighbors dies
            Assert.AreEqual(0, test.DetermineStatus(1, 0));
            //a living cell with 1 living neighbor also dies
            Assert.AreEqual(0, test.DetermineStatus(1, 1));
            //a living cell with 2 living neighbors will live on
            Assert.AreEqual(1, test.DetermineStatus(1, 2));
            //a living cell with 3 living neighbors will live on
            Assert.AreEqual(1, test.DetermineStatus(1, 3));
            //a living cell with 4 living neighbors will die
            Assert.AreEqual(0, test.DetermineStatus(1, 4));
            //a living cell with 100,000 living neighbors will die (were it a possible scenario)
            Assert.AreEqual(0, test.DetermineStatus(1, 100000));
        }
        //In cases where the user does not wish to take the time to setup the board
        //they may select the option to randomize it. This test asserts that two random
        //6x6 boards are not the same, even though the code to call them is exactly the same.
        //However, this test is not perfect, since there is a 1 in 68,719,476,736 chance that they are the same
        [TestMethod]
        public void RandomStateTest()
        {
            StateModel a = new StateModel();
            a.GenerateRandomState(6, 6);
            StateModel b = new StateModel();
            b.GenerateRandomState(6, 6);
            Assert.AreNotEqual(a, b);
        }
    }
}
