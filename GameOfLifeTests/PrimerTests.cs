﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameOfLife;

namespace GameOfLifeTests
{
    [TestClass]
    public class PrimerTests
    {
        //Ensures that constructor produces object
        [TestMethod]
        public void ConstructorTest()
        {
            Primer primer = new Primer();
            Assert.IsNotNull(primer);
        }

        //Ensures that properties can be correctly set and retrieved
        [TestMethod]
        public void PropertiesTest()
        {
            Primer primer = new Primer();
            primer.PlayerName = "Test";
            primer.Rows = 1;
            primer.Columns = 2;
            primer.LifeCycles = 3;
            Assert.AreEqual("Test", primer.PlayerName);
            Assert.AreEqual(1, primer.Rows);
            Assert.AreEqual(2, primer.Columns);
            Assert.AreEqual(3, primer.LifeCycles);
        }
    }
}
