﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GameOfLife
{
    public class Primer
    {
        public string PlayerName { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        public int LifeCycles { get; set; }
    }
}
