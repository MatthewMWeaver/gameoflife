﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class CLI
    {
        public void RunCLI()
        {
            while (true)
            {
                DisplayWelcomeMessage();
                DisplayMainMenu();
                int selection = ValidateSelection( 1, 3, "Enter Selection (1, 2 or 3): ");
                switch (selection)
                {
                    case 1:
                        DisplaySetupMenu();
                        break;
                    case 2:
                        DisplayRules();
                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                }
                Console.Clear();
            }
        }

        public void DisplayWelcomeMessage()
        {
            Console.WriteLine("Welcome to Conway's Game of Life!");
            Console.WriteLine();
            Console.WriteLine();
        }

        public void DisplayMainMenu()
        {
            Console.WriteLine("Please Select One of the Following:");
            Console.WriteLine("1. Play the Game of Life");
            Console.WriteLine("2. Show the Rules");
            Console.WriteLine("3. Exit");
            Console.WriteLine();
        }

        public int ValidateSelection(int lowerBound, int upperBound, string selectionMessage)
        {
            bool isValidUserInput = false;
            int selection = 0;
            while (!isValidUserInput)
            {
                Console.Write(selectionMessage);
                string inputString = Console.ReadLine();
                bool isNumber = Int32.TryParse(inputString, out int inputNumeral);
                if (isNumber && inputNumeral >= lowerBound && inputNumeral <= upperBound)
                {
                    selection = inputNumeral;
                    isValidUserInput = true;
                }
                else
                {
                    Console.WriteLine("Invalid Input!");
                }
            }
            return selection;
        }

        public void DisplayRules()
        {
            Console.Clear();
            Console.WriteLine("Conway's Game of Life:");
            Console.WriteLine();
            Console.WriteLine("The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician ");
            Console.WriteLine("John Horton Conway in 1970. The universe of the Game of Life is a two-dimensional orthogonal grid of cells.");
            Console.WriteLine("Cells each are in one of two possible states, alive or dead, or populated or unpopulated. At each life cycle,");
            Console.WriteLine("the following transitions occur:");
            Console.WriteLine();
            Console.WriteLine("1. Any live cell with fewer than two live neighbors dies, as if caused by under-population.");
            Console.WriteLine("2. Any live cell with more than three live neighbors dies, as if by overcrowding.");
            Console.WriteLine("3. Any live cell with two or three live neighbors lives on to the next generation.");
            Console.WriteLine("4. Any dead cell with exactly three live neighbors becomes a live cell.");
            Console.WriteLine();
            Console.WriteLine("A cell’s neighbors are those cells which are horizontally, vertically or diagonally adjacent.");
            Console.WriteLine("Most cells will have eight neighbors. Cells placed on the edge of the grid will have fewer.");
            Console.WriteLine();
            Console.WriteLine("Press any key to return to Main Menu");
            Console.ReadLine();
        }

        public void DisplaySetupMenu()
        {
            Console.Clear();
            StateModel initialState = GatherSetupInformation();
            RunGameOfLife(initialState);
        }

        public StateModel GatherSetupInformation()
        {
            Console.WriteLine("Game Setup");
            Console.WriteLine();
            Primer setup = GatherSetup();
            Console.WriteLine();
            Console.WriteLine("Select Setup Options:");
            Console.WriteLine("1. Create your own board");
            Console.WriteLine("2. Use a random Seed");
            int userSelection = ValidateSelection(1, 2, "Select Setup Option: ");
            StateModel initialState;
            if (userSelection == 1)
            {
                initialState = GatherUserInputtedState(setup);
            }
            else
            {
                initialState = new StateModel(setup);
                initialState.GenerateRandomState(setup.Rows, setup.Columns);
            }
            return initialState;
        }

        public Primer GatherSetup()
        {
            Primer setup = new Primer();
            Console.Write("Please Enter Your Name:");
            setup.PlayerName = Console.ReadLine();
            setup.Rows = ValidateSelection(4, 24, "Please enter the number of rows on the board (at least 4, up to 24): ");
            setup.Columns = ValidateSelection(4, 24, "Please enter the number of columns on the board (at least 4, up to 24): ");
            setup.LifeCycles = ValidateSelection(2, 12, "Please enter the number of Life Cycles you wish to model (at least 2, up to 12): ");
            return setup;
        }

        public StateModel GatherUserInputtedState(Primer setup)
        {
            Console.Clear();
            Console.WriteLine("Game Setup");
            Console.WriteLine();
            Console.WriteLine($"Ok, let's build your initial {setup.Rows} by {setup.Columns} Life State. Enter");
            Console.WriteLine($"each row of {setup.Columns} inputs one at a time, enter '0' to represent a living");
            Console.WriteLine("cell and '.' to represent a dead cell. No other character inputs will be accepted.");
            Console.WriteLine();
            StateModel initial = new StateModel(setup);
            for (int i = 0; i < setup.Rows; i++)
            {
                List<int> row = GetRow(setup, i+1);
                initial.Cells.Add(row);
            }
            return initial;
        }

        public List<int> GetRow(Primer setup, int rowNumber)
        {
            string rowString = "";
            bool isValidRow = false;
            while (!isValidRow)
            {
                Console.Write($"Enter row {rowNumber}: ");
                string inputString = Console.ReadLine();
                if (inputString.Length == setup.Columns)
                {
                    bool invalidCharacter = false;
                    for (int i = 0; i < inputString.Length; i++)
                    {
                        if (inputString.ElementAt(i) != '.' && inputString.ElementAt(i) != '0'  )
                        {
                            invalidCharacter = true;
                        }
                    }
                    if (invalidCharacter)
                    {
                        Console.WriteLine("Invalid Input!");
                    }
                    else
                    {
                        isValidRow = true;
                        rowString = inputString;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Input!");
                }
            }
            List<int> row = ProcessRow(rowString);
            return row;
        }

        public List<int> ProcessRow(string rowString)
        {
            List<int> row = new List<int>();
            foreach (char cell in rowString)
            {
                if (cell == '.')
                {
                    row.Add(0);
                }
                else if (cell == '0')
                {
                    row.Add(1);
                }
            }
            return row;
        }

        public void RunGameOfLife(StateModel initialState)
        {
            List<StateModel> states = new List<StateModel>();
            states.Add(initialState);
            StateModel previousGeneration = initialState;
            for (int i = 0; i < initialState.LifeCycles; i++)
            {
                StateModel nextGeneration = new StateModel(previousGeneration);
                states.Add(nextGeneration);
                previousGeneration = nextGeneration;
            }
            int count = 0;
            foreach (StateModel state in states)
            {
                count++;
                DisplayState(state, count);
                Console.WriteLine();
                Console.Write("Displaying next state in 5");
                for (int i = 4; i > 0; i--)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        System.Threading.Thread.Sleep(250);
                        Console.Write(".");
                    }
                    System.Threading.Thread.Sleep(250);
                    Console.Write(i);
                }
                System.Threading.Thread.Sleep(500);
            }
            Console.WriteLine();
            Console.WriteLine("Press any key to exit simulation and return to Main Menu");
            Console.ReadLine();
        }

        public void DisplayState(StateModel state, int count)
        {
            Console.Clear();
            Console.WriteLine($"{state.PlayerName}'s Game of Life");
            Console.WriteLine();
            Console.WriteLine($"Generation {count}:");
            foreach (List<int> row in state.Cells)
            {
                Console.Write("        ");
                foreach (int cell in row)
                {
                    if (cell == 0)
                    {
                        Console.Write(".");
                    }
                    else if (cell == 1)
                    {
                        Console.Write("0");
                    }
                }
                Console.Write(Environment.NewLine);
            }
        }
    }
}
