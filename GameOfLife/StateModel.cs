﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class StateModel
    {
        public string PlayerName { get; set; }
        public int LifeCycles { get; set; }
        public List<List<int>> Cells { get; set; }

        public StateModel()
        {
            Cells = new List<List<int>>();
        }

        public StateModel(Primer specs)
        {
            Cells = new List<List<int>>();
            PlayerName = specs.PlayerName;
            LifeCycles = specs.LifeCycles;
        }

        public void GenerateRandomState(int rows, int columns)
        {
            if (Cells.Count == 0)
            {
                Random random = new Random();
                for (int i = 0; i < rows; i++)
                {
                    List<int> row = new List<int>();
                    for (int j = 0; j < columns; j++)
                    {
                        row.Add(random.Next(0, 2));
                    }
                    Cells.Add(row);
                }
            }
        }

        public StateModel(StateModel initalState)
        {
            PlayerName = initalState.PlayerName;
            Cells = new List<List<int>>();
            for (int i = 0; i < initalState.Cells.Count; i++)
            {
                List<int> row = new List<int>();
                for (int j = 0; j < initalState.Cells[0].Count; j++)
                {
                    int livingNeighbors = CountLivingNeighbors(i, j, initalState);
                    int status = DetermineStatus(initalState.Cells[i][j], livingNeighbors);
                    row.Add(status);
                }
                Cells.Add(row);
            }
        }

        public int CountLivingNeighbors(int i, int j, StateModel initialState)
        {
            int livingNeighbors = 0;
            bool isRightmostColumn = (j == initialState.Cells[0].Count - 1);
            bool isLeftmostColumn = (j == 0);
            bool isTopRow = (i == 0);
            bool isBottomRow = (i == initialState.Cells.Count - 1);
            if (isTopRow && isLeftmostColumn)
            {
                livingNeighbors += initialState.Cells[i][j + 1];
                livingNeighbors += initialState.Cells[i + 1][j];
                livingNeighbors += initialState.Cells[i + 1][j + 1];
            }
            else if (isTopRow && isRightmostColumn)
            {
                livingNeighbors += initialState.Cells[i][j - 1];
                livingNeighbors += initialState.Cells[i + 1][j - 1];
                livingNeighbors += initialState.Cells[i + 1][j];
            }
            else if (isBottomRow && isLeftmostColumn)
            {
                livingNeighbors += initialState.Cells[i - 1][j];
                livingNeighbors += initialState.Cells[i - 1][j + 1];
                livingNeighbors += initialState.Cells[i][j + 1];
            }
            else if (isBottomRow && isRightmostColumn)
            {
                livingNeighbors += initialState.Cells[i - 1][j - 1];
                livingNeighbors += initialState.Cells[i - 1][j];
                livingNeighbors += initialState.Cells[i][j - 1];
            }
            else if (isLeftmostColumn)
            {
                livingNeighbors += initialState.Cells[i - 1][j];
                livingNeighbors += initialState.Cells[i - 1][j + 1];
                livingNeighbors += initialState.Cells[i][j + 1];
                livingNeighbors += initialState.Cells[i + 1][j];
                livingNeighbors += initialState.Cells[i + 1][j + 1];
            }
            else if (isTopRow)
            {
                livingNeighbors += initialState.Cells[i][j - 1];
                livingNeighbors += initialState.Cells[i][j + 1];
                livingNeighbors += initialState.Cells[i + 1][j - 1];
                livingNeighbors += initialState.Cells[i + 1][j];
                livingNeighbors += initialState.Cells[i + 1][j + 1];
            }
            else if (isRightmostColumn)
            {
                livingNeighbors += initialState.Cells[i - 1][j - 1];
                livingNeighbors += initialState.Cells[i - 1][j];
                livingNeighbors += initialState.Cells[i][j - 1];
                livingNeighbors += initialState.Cells[i + 1][j - 1];
                livingNeighbors += initialState.Cells[i + 1][j];
            }
            else if (isBottomRow)
            {
                livingNeighbors += initialState.Cells[i - 1][j - 1];
                livingNeighbors += initialState.Cells[i - 1][j];
                livingNeighbors += initialState.Cells[i - 1][j + 1];
                livingNeighbors += initialState.Cells[i][j - 1];
                livingNeighbors += initialState.Cells[i][j + 1];
            }
            else
            {
                livingNeighbors += initialState.Cells[i - 1][j - 1];
                livingNeighbors += initialState.Cells[i - 1][j];
                livingNeighbors += initialState.Cells[i - 1][j + 1];
                livingNeighbors += initialState.Cells[i][j - 1];
                livingNeighbors += initialState.Cells[i][j + 1];
                livingNeighbors += initialState.Cells[i + 1][j - 1];
                livingNeighbors += initialState.Cells[i + 1][j];
                livingNeighbors += initialState.Cells[i + 1][j + 1];
            }
            return livingNeighbors;
        }

        public int DetermineStatus(int currentStatus, int livingNeighbors)
        {
            if (currentStatus == 1)
            {
                if (livingNeighbors == 2 || livingNeighbors == 3)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                if (livingNeighbors == 3)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
